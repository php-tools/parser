#!/usr/bin/env ruby
# Encoding: utf-8
# frozen_string_literal: true

# Main class for the php parser
class Parser
  require 'thread'
  require_relative '../lexer/Lexer.rb'

  def initialize(debug)
    puts 'New Parser initialized' if debug
    @debug = debug
  end

  def start(filename)
    l     = Lexer.new @debug
    queue = Queue.new
    l.start filename, queue
    # output
  end

  def process_tokens(queue)
    Thread.new do
      while loop
        token = queue.pop
        puts token
        break if token.nil?
      end
    end
  end
end
